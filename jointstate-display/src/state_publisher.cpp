#include <string>
#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <tf/transform_broadcaster.h>



int main(int argc, char** argv) {
    ros::init(argc, argv, "state_publisher");
    ros::NodeHandle n;
    ros::Publisher joint_pub = n.advertise<sensor_msgs::JointState>("joint_states", 1);
    tf::TransformBroadcaster broadcaster;
    ros::Rate loop_rate(30);

    const double degree = M_PI/180;

    // robot state
    double tilt,tilt2 = 0, tinc = degree, swivel=0, angle=0, height=0, hinc=0.005;

    // message declarations
    geometry_msgs::TransformStamped odom_trans;
    sensor_msgs::JointState joint_state;
    odom_trans.header.frame_id = "odom";
    odom_trans.child_frame_id = "Body_Torso";

    while (ros::ok()) {
        //update joint_state
        joint_state.header.stamp = ros::Time::now();
        joint_state.name.resize(6);
        joint_state.position.resize(6);
        joint_state.name[0] ="HPY";
        joint_state.position[0] = swivel;
        joint_state.name[1] ="LSP";
        joint_state.position[1] = tilt*0.5;
        joint_state.name[2] ="RSP";
        joint_state.position[2] = tilt2*0.5;
        joint_state.name[3] ="LHP";
        joint_state.position[3] = tilt2;
        joint_state.name[4] ="RHP";
        joint_state.position[4] = tilt;
        joint_state.name[5] ="periscope";
        joint_state.position[5] = height;


        // update transform
        // (moving in a circle with radius=2)
	/*
        odom_trans.header.stamp = ros::Time::now();
        odom_trans.transform.translation.x = sin(angle)*2;
        odom_trans.transform.translation.y = cos(angle)*2;
        odom_trans.transform.translation.z = .7;
        odom_trans.transform.rotation = tf::createQuaternionMsgFromYaw(angle+M_PI/2);
	*/

        //send the joint state and transform
        joint_pub.publish(joint_state);
        broadcaster.sendTransform(odom_trans);

        // Create new robot state
        tilt += tinc;
        tilt2 -= tinc;
        swivel +=tinc;
        if (tilt<-.5 || tilt>.5) tinc *= -1;
        height += hinc;
        if (height>.2 || height<0) hinc *= -1;

        //rotate
        //swivel += degree;
        angle += degree/4;

        // This will adjust as needed per iteration
        loop_rate.sleep();
    }


    return 0;
}
